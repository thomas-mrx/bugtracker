<?php
$locale = explode(".",setlocale(LC_CTYPE, 0))[0];
?>
<div class="bt_container">
    <div class="bt_row">
        <div class="bt_col">
            <?= \BugTracker\Lang::get($locale,"issues_sent") ?>
        </div>
        <div class="bt_col">
            <?= \BugTracker\Lang::get($locale,"issues_doing") ?>
        </div>
        <div class="bt_col">
            <?= \BugTracker\Lang::get($locale,"issues_done") ?>
        </div>
    </div>
</div>