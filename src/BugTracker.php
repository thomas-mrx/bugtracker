<?php
namespace BugTracker;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class BugTracker {

    private $httpClient;
    private $IssueManager = null;
    private $HTMLRenderer = null;

    public function __construct(
        string $accessToken,
        string $projectID,
        string $gitlabDomain = "https://gitlab.com"
    ) {
        $this->httpClient = new Client(
            [
                'base_uri' => $gitlabDomain . "/api/v4/projects/" . $projectID . "/",
                'headers' => [
                    'Content-Type' => 'application/json',
                    'PRIVATE-TOKEN' => $accessToken
                ]
            ]
        );
        $this->checkCredentials();
    }

    public function checkCredentials(): bool
    {
        try {
            $this->httpClient->get("");
        } catch (ClientException $e) {
            trigger_error("Configuration error detected while checking credentials. ".$e->getMessage(), E_USER_ERROR);
        }
        return true;
    }

    /**
     * @return IssuesManager
     */
    public function getIssuesManager(): IssuesManager
    {
        if ($this->IssueManager == null) {
            $this->IssueManager = new IssuesManager($this->httpClient);
        }
        return $this->IssueManager;
    }

    /**
     * @return HTMLRenderer
     */
    public function getHTMLRenderer(): HTMLRenderer
    {
        if ($this->HTMLRenderer == null) {
            $this->HTMLRenderer = new HTMLRenderer($this->getIssuesManager());
        }
        return $this->HTMLRenderer;
    }


}
