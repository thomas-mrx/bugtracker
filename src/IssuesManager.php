<?php
namespace BugTracker;

use GuzzleHttp\Exception\ClientException;

class IssuesManager {

    private $issues = [];
    private $http;

    public function __construct(
        \GuzzleHttp\Client $http
    ) {
        $this->http = $http;
        $response = $this->http->get("issues");
        if ($data = $response->getBody()) {
            $this->issues = json_decode($data);
        }
    }

    public function getAll() {
        return $this->issues;
    }

    public function new($title, $description = "")
    {
        try{
            $response = $this->http->post('issues', [
                'form_params' => [
                    'title' => $title,
                    'description' => $description,
                    'labels' => 'bug'
                ]
            ]);
            if ($response->getStatusCode() != 201) {
                throw new \RuntimeException("API doesn't return expected status code. Expected 201, got ".$response->getStatusCode().".");
            }
        } catch (\RuntimeException | ClientException $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return false;
        }
        $issue = json_decode($response->getBody()->getContents());
        $this->issues[] = $issue;
        return $issue;
    }

    public function delete($iid)
    {
        try{
            $response = $this->http->delete("issues/$iid");
            if ($response->getStatusCode() != 204) {
                throw new \RuntimeException("API doesn't return expected status code. Expected 204, got ".$response->getStatusCode().".");
            }
        } catch (\RuntimeException | ClientException $e) {
            trigger_error($e->getMessage(), E_USER_WARNING);
            return false;
        }
        $this->issues = array_filter($this->issues,function($i) use ($iid) {return $i->iid != $iid;});
        return true;
    }

}
