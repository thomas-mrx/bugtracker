<?php
namespace BugTracker;


class HTMLRenderer {

    private $issues;
    private $cssLoaded = false;

    public function __construct(
        IssuesManager $issues
    ) {
        $this->issues = $issues;
    }

    public function dashboard($filter = null){
        if ($filter === null) {
            $filter = function($arg) { return !empty($arg); };
        }
        $filteredIssues = array_filter($this->issues->getAll(),$filter);
        include dirname(__DIR__) . '/templates/dashboard.php';
        $this->loadCSS();
    }

    private function loadCSS(){
        if(!$this->cssLoaded){
            echo '<style>';
            include dirname(__DIR__) . '/assets/css/style.css';
            echo '</style>';
            $this->cssLoaded = true;
        }
    }

}
