<?php
namespace BugTracker;

class Lang {

    static private $translations = [
        "en_US" => [
            "issues_sent" => "Sent",
            "issues_doing" => "Doing",
            "issues_done" => "Done"
        ],
        "fr_FR" => [
            "issues_sent" => "Transmises",
            "issues_doing" => "En cours",
            "issues_done" => "Terminées"
        ]
    ];

    static public function get($code, $identifier): string
    {
        return Lang::$translations[$code][$identifier] ?? Lang::$translations["en_US"][$identifier] ?? trigger_error("No matching translation for '$identifier'.");
    }

}