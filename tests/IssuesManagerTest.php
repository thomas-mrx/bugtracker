<?php

use BugTracker\BugTracker;
use BugTracker\IssuesManager;
use PHPUnit\Framework\TestCase;

class IssuesManagerTest extends TestCase {

    public function testLoadTestConfig()
    {
        /* for local testing - put .env file in /tests directory */
        if (empty($_ENV['ACCESS_TOKEN']) || empty($_ENV['PROJECT_ID'])) {
            $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
            $dotenv->load();
        }

        $this->assertNotEmpty($_ENV['ACCESS_TOKEN']);
        $this->assertNotEmpty($_ENV['PROJECT_ID']);
    }

    /**
     * @depends testLoadTestConfig
     */
    public function testInstantiationOfBugTracker(): BugTracker
    {
        $bugtracker = new BugTracker($_ENV['ACCESS_TOKEN'], $_ENV['PROJECT_ID']);
        $this->assertInstanceOf('BugTracker\BugTracker',$bugtracker);
        return $bugtracker;
    }

    /**
     * @depends testInstantiationOfBugTracker
     */
    public function testIssuesLoading(BugTracker $tracker): IssuesManager
    {
        $issuesmanager = $tracker->getIssuesManager();
        $this->assertInstanceOf('BugTracker\IssuesManager', $issuesmanager);
        return $issuesmanager;
    }

    /**
     * @depends testIssuesLoading
     */
    public function testIssuesGetAll(IssuesManager $issues)
    {
        $result = $issues->getAll();
        $this->assertIsArray($result);
    }

    /**
     * @depends testIssuesLoading
     */
    public function testIssuesAddOne(IssuesManager $issues): array
    {
        $issue = $issues->new("toto");
        $this->assertIsObject($issue);
        return [$issues, $issue];
    }

    /**
     * @depends testIssuesAddOne
     */
    public function testIssuesRemoveOne($args)
    {
        $result = $args[0]->delete($args[1]->iid);
        $this->assertTrue($result);
    }

    /**
     * @depends testIssuesLoading
     *
     */
    public function testIssuesAddWrong(IssuesManager $issues)
    {
        $result = @$issues->new("too long",bin2hex(random_bytes(600000)));
        $this->assertFalse($result);
    }

    /**
     * @depends testIssuesLoading
     */
    public function testIssuesRemoveWrong(IssuesManager $issues)
    {
        $result = @$issues->delete("fake_iid");
        $this->assertFalse($result);
    }
}