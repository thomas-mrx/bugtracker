<?php

use BugTracker\BugTracker;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Error;

class BugTrackerTest extends TestCase {

    public function testLoadTestConfig()
    {
        /* for local testing - put .env file in /tests directory */
        if (empty($_ENV['ACCESS_TOKEN']) || empty($_ENV['PROJECT_ID'])) {
            $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
            $dotenv->load();
        }

        $this->assertNotEmpty($_ENV['ACCESS_TOKEN']);
        $this->assertNotEmpty($_ENV['PROJECT_ID']);
    }

    public function testWrongCredentials()
    {
        $this->expectException(Error\Error::class);
        new BugTracker("fake token", "fake id");
    }

    /**
     * @depends testLoadTestConfig
     */
    public function testInstantiationOfBugTracker(): BugTracker
    {
        $bugtracker = new BugTracker($_ENV['ACCESS_TOKEN'], $_ENV['PROJECT_ID']);
        $this->assertInstanceOf('BugTracker\BugTracker',$bugtracker);
        return $bugtracker;
    }
}