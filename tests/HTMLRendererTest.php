<?php

use BugTracker\BugTracker;
use BugTracker\HTMLRenderer;
use BugTracker\IssuesManager;
use PHPUnit\Framework\TestCase;

class HTMLRendererTest extends TestCase {

    public function testLoadTestConfig()
    {
        /* for local testing - put .env file in /tests directory */
        if (empty($_ENV['ACCESS_TOKEN']) || empty($_ENV['PROJECT_ID'])) {
            $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
            $dotenv->load();
        }

        $this->assertNotEmpty($_ENV['ACCESS_TOKEN']);
        $this->assertNotEmpty($_ENV['PROJECT_ID']);
    }

    /**
     * @depends testLoadTestConfig
     */
    public function testInstantiationOfBugTracker(): BugTracker
    {
        $bugtracker = new BugTracker($_ENV['ACCESS_TOKEN'], $_ENV['PROJECT_ID']);
        $this->assertInstanceOf('BugTracker\BugTracker',$bugtracker);
        return $bugtracker;
    }

    /**
     * @depends testInstantiationOfBugTracker
     */
    public function testHTMLLoading(BugTracker $tracker): HTMLRenderer
    {
        $html = $tracker->getHTMLRenderer();
        $this->assertInstanceOf('BugTracker\HTMLRenderer', $html);
        return $html;
    }

    /**
     * @depends testHTMLLoading
     */
    public function testHTMLDashboard(HTMLRenderer $html)
    {
        $this->expectOutputRegex('/\w+/');
        $html->dashboard();
    }
}